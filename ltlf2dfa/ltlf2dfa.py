# -*- coding: utf-8 -*-

"""Main module of the pakage."""

import itertools as it
from subprocess import PIPE, Popen, TimeoutExpired
import os
import re
import time
import signal
from functools import reduce

from sympy import symbols, And, Not, Or, simplify, Symbol
from sympy.logic.boolalg import to_dnf, BooleanFalse, BooleanTrue

from ltlf2dfa.base import MonaProgram

PACKAGE_DIR = os.path.dirname(os.path.abspath(__file__))

UNSAT_DOT = """digraph MONA_DFA {
 rankdir = LR;
 center = true;
 size = "7.5,10.5";
 edge [fontname = Courier];
 node [height = .5, width = .5];
 node [shape = doublecircle];
 node [shape = circle]; 1;
 init [shape = plaintext, label = ""];
 init -> 1;
 1 -> 1 [label="true"];
}"""


def get_value(text, regex, value_type=float):
	"""Dump a value from a file based on a regex passed in."""
	pattern = re.compile(regex, re.MULTILINE)
	results = pattern.search(text)
	if results:
		return value_type(results.group(1))
	else:
		print("Could not find the value {}, in the text provided".format(regex))
		return value_type(0.0)


def ter2symb(ap, ternary):
	"""Translate ternary output to symbolic."""
	expr = And()
	i = 0
	for value in ternary:
		if value == "1":
			expr = And(expr, ap[i] if isinstance(ap, tuple) else ap)
		elif value == "0":
			assert value == "0"
			expr = And(expr, Not(ap[i] if isinstance(ap, tuple) else ap))
		else:
			assert value == "X", "[ERROR]: the guard is not X"
		i += 1
	return expr


def simplify_guard(guards):
	"""Make a big OR among guards and simplify them."""
	final = Or()
	for g in guards:
		final = Or(final, g)
	return simplify(final)


def parse_mona(mona_output):
	"""Parse mona output and construct a dot."""
	free_variables = get_value(
		mona_output, r".*DFA for formula with free variables:[\s]*(.*?)\n.*", str
	)
	if "state" in free_variables:
		free_variables = None
	else:
		free_variables = symbols(
			" ".join(
				x.strip().lower() for x in free_variables.split() if len(x.strip()) > 0
			)
		)

	# initial_state = get_value(mona_output, '.*Initial state:[\s]*(\d+)\n.*', int)
	accepting_states = get_value(mona_output, r".*Accepting states:[\s]*(.*?)\n.*", str)
	accepting_states = [
		str(x.strip()) for x in accepting_states.split() if len(x.strip()) > 0
	]
	# num_states = get_value(mona_output, '.*Automaton has[\s]*(\d+)[\s]states.*', int) - 1

	dot = """digraph MONA_DFA {
 rankdir = LR;
 center = true;
 size = "7.5,10.5";
 edge [fontname = Courier];
 node [height = .5, width = .5];\n"""
	dot += " node [shape = doublecircle]; {};\n".format("; ".join(accepting_states))
	dot += """ node [shape = circle]; 1;
 init [shape = plaintext, label = ""];
 init -> 1;\n"""

	dot_trans = dict()  # maps each couple (src, dst) to a list of guards
	for line in mona_output.splitlines():
		if line.startswith("State "):
			orig_state = get_value(line, r".*State[\s]*(\d+):\s.*", int)
			guard = get_value(line, r".*:[\s](.*?)[\s]->.*", str)
			if free_variables:
				guard = ter2symb(free_variables, guard)
			else:
				guard = ter2symb(free_variables, "X")
			dest_state = get_value(line, r".*state[\s]*(\d+)[\s]*.*", int)
			if orig_state:
				if (orig_state, dest_state) in dot_trans.keys():
					dot_trans[(orig_state, dest_state)].append(guard)
				else:
					dot_trans[(orig_state, dest_state)] = [guard]

	for c, guards in dot_trans.items():
		simplified_guard = simplify_guard(guards)
		dot += ' {} -> {} [label="{}"];\n'.format(
			c[0], c[1], str(simplified_guard).lower()
		)

	dot += "}"
	return dot



###############################################################################
### FOR ASP PROJECT




def parse_mona_to_facts(mona_output, automaton_number, mode):
	"""Parse mona output and construct FACTS."""

	# Get the free variables, but get the wrong line if there are not.
	free_variables = get_value(
		mona_output, r".*DFA for formula with free variables:[\s]*(.*?)\n.*", str
	)
	# If there is not free variables, then there is none ;)
	if "state" in free_variables:
		free_variables = None
	else:
		#construct a tuple of free variables
		free_variables = symbols(
			" ".join(
				x.strip().lower() for x in free_variables.split() if len(x.strip()) > 0
			)
		)


	# For the initial state, we must not take 0, as the mona output indicates, but 1, because of the way of handling boolean by mona.
	initial_state = 1


	# Get the accepting states. There is at least one, otherwise this funciton would never be called. 
	accepting_states = get_value(mona_output, r".*Accepting states:[\s]*(.*?)\n.*", str)
	accepting_states = [
		str(x.strip()) for x in accepting_states.split() if len(x.strip()) > 0
	]
	
	# Get the rejecting states. Does not work if there is no rejecting state.
	# rejecting_states = get_value(mona_output, r".*Rejecting states:[\s]*(.*?)\n.*", str)
	# rejecting_states = [
	# 	str(x.strip()) for x in rejecting_states.split() if len(x.strip()) > 0
	# ]
	# if rejecting_states[0] == "Don't-care" : 
	# 	rejecting_states = []

	#number of states
	num_states = get_value(mona_output, '.*Automaton has[\s]*(\d+)[\s]states.*', int) - 1

	## just to solve iterability of variables.
	iterable_free_variables = free_variables
	if isinstance(iterable_free_variables, Symbol) : 
		iterable_free_variables = [iterable_free_variables]
	if type(iterable_free_variables) == type(None) : 
		iterable_free_variables = []

	# Creating the output, composed of facts
	facts = ""
	
	facts += "automaton({}).\n\n".format(automaton_number)

	if len(iterable_free_variables) > 0 :
		facts += "prop({}, ({})).\n\n".format(automaton_number, " ; ".join(sorted([str(symbol) for symbol in iterable_free_variables])))

	facts += "state({}, 1..{}).\n\n".format(automaton_number, num_states)

	facts += "s0({}, {}).\n\n".format(automaton_number, initial_state)

	facts += "accepting({}, ({})).\n\n".format(automaton_number, " ; ".join(accepting_states))

	# Creating a dict
	dot_trans = dict()  # maps each couple (src, dst) to a list of guards
	for line in mona_output.splitlines():
		# If line explains a transition
		if line.startswith("State "):
			orig_state = get_value(line, r".*State[\s]*(\d+):\s.*", int)
			guard = get_value(line, r".*:[\s](.*?)[\s]->.*", str)
			#print(orig_state)
			#print(guard)
			if free_variables:
				guard = ter2symb2(free_variables, guard)
			else:
				guard = ter2symb2(free_variables, "X")
			#print(guard)
			#print(type(guard))
			dest_state = get_value(line, r".*state[\s]*(\d+)[\s]*.*", int)

			if orig_state:
				if (orig_state, dest_state) in dot_trans.keys():
					dot_trans[(orig_state, dest_state)].append(guard)
				else:
					dot_trans[(orig_state, dest_state)] = [guard]

	looping_states = []
	for (orig_state, dest_state), guards in dot_trans.items():
		for guard in guards :
			#could be cleaner with a dict of possible next states, but I think that this works.
			if  orig_state == dest_state and isinstance(guard, BooleanTrue): # If the state is a looping state
				looping_states.append(str(orig_state))

	if mode == 1 : 
		#Creating the delta function
		delta_dynamic = "\n#program dynamic.\n"
		delta_initial = "\n#program initial.\n"
		for (orig_state, dest_state), guards in dot_trans.items():

			## Leads to a simpler form but takes much time (like seconds on instance of 10 variables)
			# I_want_to_simplify_the_guards = False
			# if(I_want_to_simplify_the_guards) : 
			# 	simplified_guard = simplify_guard(guards)
			# 	dnf_guard = to_dnf(simplified_guard)
			# 
			# 	if isinstance(dnf_guard, Or) : 
			# 		for guard in dnf_guard.args : 
			# 			delta_dynamic += "delta({}, {}, {}) :- 'current_state({}, {}){}.\n".format(automaton_number, orig_state, dest_state, automaton_number, orig_state, guard_to_facts(guard))
			# 	else :
			# 		delta_dynamic += "delta({}, {}, {}) :- 'current_state({}, {}){}.\n".format(automaton_number, orig_state, dest_state, automaton_number, orig_state, guard_to_facts(guard))
			# else : 
				for guard in guards :
					# #could be cleaner with a dict of possible next states, but I think that this works.
					# if  orig_state == dest_state and isinstance(guard, BooleanTrue): # If the state is a looping state
					# 	looping_states.append(str(orig_state))
					delta_dynamic += "delta({}, {}, {}) :- 'current_state({}, {}){}.\n".format(automaton_number, orig_state, dest_state, automaton_number, orig_state, guard_to_telingo_facts(guard))
					if orig_state == initial_state : 
						delta_initial += "delta({}, {}) :- {}.\n".format(automaton_number, dest_state, guard_to_telingo_facts(guard)[2:])
			# else : 
			# 	for guard in guards : 
			# 		facts += "delta({}, {}, A, {}) :- alphabet(A){}.\n".format(automaton_number, orig_state, dest_state, guard_to_facts(guard))


		#p6 = time.time()
		#print("\n-----time from p5 to p6 : " + str(p6 - p5) + "s.\n")
		facts += delta_initial
		facts += delta_dynamic
	

	elif mode == 0: 
		delta_dynamic = "\n"
		delta_initial = "\n"
		for (orig_state, dest_state), guards in dot_trans.items():
			for guard in guards :
				# #could be cleaner with a dict of possible next states, but I think that this works.
				# if  orig_state == dest_state and isinstance(guard, BooleanTrue): # If the state is a looping state
				# 	looping_states.append(str(orig_state))
				delta_dynamic += "delta({}, {}, {}, T) :- T=1..horizon, current_state({}, {}, T-1){}.\n".format(automaton_number, orig_state, dest_state, automaton_number, orig_state, guard_to_clingo_facts(guard))
				if orig_state == initial_state : 
					delta_initial += "delta({}, {}, T) :- T=0{}.\n".format(automaton_number, dest_state, guard_to_clingo_facts(guard))

		facts += delta_initial
		facts += delta_dynamic


	elif mode == 2: 
		delta_dynamic = "\n"
		delta_initial = "\n"
		for (orig_state, dest_state), guards in dot_trans.items():
			for guard in guards :
				# #could be cleaner with a dict of possible next states, but I think that this works.
				# if  orig_state == dest_state and isinstance(guard, BooleanTrue): # If the state is a looping state
				# 	looping_states.append(str(orig_state))
				delta_dynamic += "delta({}, {}, {}, T) :- T=1..horizon, current_state({}, {}, T-1){}.\n".format(automaton_number, orig_state, dest_state, automaton_number, orig_state, guard_to_clingo_facts_v2(guard))
				if orig_state == initial_state : 
					delta_initial += "delta({}, {}, T) :- T=0{}.\n".format(automaton_number, dest_state, guard_to_clingo_facts_v2(guard))

		facts += delta_initial
		facts += delta_dynamic

	
	elif mode == 3:# or mode == 4: 

		delta = "\n"
		map_edge_to_guards = {}
		for (orig_state, dest_state), guards in dot_trans.items():
			map_edge_to_guards[(orig_state,dest_state)] = []
			for guard in guards :
				# #could be cleaner with a dict of possible next states, but I think that this works.
				# if  orig_state == dest_state and isinstance(guard, BooleanTrue): # If the state is a looping state
				# 	looping_states.append(str(orig_state))
				# delta += "delta({}, {}, {}) :- satisfies({}).\n".format(automaton_number, orig_state, dest_state, guard_to_clingo_facts_v3(guard))
				clingo_fact_from_this_guard = guard_to_clingo_facts_v3(guard)
				map_edge_to_guards[orig_state,dest_state].append(clingo_fact_from_this_guard)
				
		for ((orig_state, dest_state), facts_of_guards) in map_edge_to_guards.items() : 
			# print("ORIG_STATE, DEST_STATE, facts_of_guards :" )
			# print(((orig_state, dest_state), facts_of_guards))

			disjunction_of_facts = reduce(lambda x,y : "or({}, {})".format(x, y), facts_of_guards)
			formula_from_guards = "{}".format(disjunction_of_facts)
			delta += "delta({}, {}, {}, {}).\n".format(automaton_number, orig_state, dest_state, formula_from_guards)


		facts += delta

		holccurs_atoms = "\n"
		for symbol in sorted([str(symbol) for symbol in iterable_free_variables]) :
 			holccurs_atoms += "holccurs({}, T) :- {}TIMEHERE.\n".format(symbol, symbol)
		holccurs_atoms += "\n"
		facts += holccurs_atoms

	elif mode == 4: 
		delta = "\n"
		for (orig_state, dest_state), guards in dot_trans.items():
			# for guard in guards :
			# 	#could be cleaner with a dict of possible next states, but I think that this works.
			# 	if  orig_state == dest_state and isinstance(guard, BooleanTrue): # If the state is a looping state
			# 		looping_states.append(str(orig_state))
			# Leads to a simpler form but takes much time (like seconds on instance of 10 variables)
			simplified_guard = simplify_guard(guards)
			# dnf_guard = to_dnf(simplified_guard)
			delta += "delta({}, {}, {}, {}).\n".format(automaton_number, orig_state, dest_state, guard_to_clingo_facts_v3(simplified_guard))

		facts += delta

		holccurs_atoms = "\n"
		for symbol in sorted([str(symbol) for symbol in iterable_free_variables]) :
 			holccurs_atoms += "holccurs({}, T) :- {}TIMEHERE.\n".format(symbol, symbol)
		holccurs_atoms += "\n"
		facts += holccurs_atoms


	else :#if mode == 5: 
		delta = "\n"
		delta_initial = "\n"
		s0_can_loop_on_itself = False
		for (orig_state, dest_state), guards in dot_trans.items():
			if orig_state == dest_state == initial_state : 
				s0_can_loop_on_itself = True
		for (orig_state, dest_state), guards in dot_trans.items():
			for guard in guards :
				if orig_state == initial_state : 
					delta_initial += "current_state_{n}({current_state}, 0) :- T=0{cond}.\n".format(n=automaton_number, current_state=dest_state, cond=guard_to_clingo_facts_v2(guard))
				if orig_state != initial_state or s0_can_loop_on_itself:  # ELSE OR
					delta += "current_state_{n}({current_state}, T) :- T=1..horizon, current_state_{n}({old_state}, T-1){cond}.\n".format( n=automaton_number, current_state=dest_state, old_state=orig_state, cond=guard_to_clingo_facts_v2(guard))
				
				
		facts += delta_initial
		facts += delta

		accept_reject_facts = "\naccepting_run_{n} :- current_state_{n}(S, horizon), accepting({n}, S).\n".format(n=automaton_number)
		accept_reject_facts += ":- not accepting_run_{n}.\n".format(n=automaton_number)
		accept_reject_facts += ":- current_state_{n}(S, T), rejecting_state({n}, S).\n".format(n=automaton_number)
		
		facts += accept_reject_facts


	if len(looping_states) > 0 : 
		facts_looping = "\n"
		if mode == 1 : 
			facts_looping += "#program intial.\n"
		facts_looping += "looping_state({}, ({})).\n".format(automaton_number, " ; ".join(looping_states))
		facts += facts_looping
		rejecting_states = [n for n in looping_states if n not in accepting_states]
		if len(rejecting_states) > 0 : 
			facts_rejecting = "\n"
			if mode == 1 : 
				facts_rejecting += "#program initial.\n"
			facts_rejecting += "\nrejecting_state({}, ({})).\n".format(automaton_number, " ; ".join(rejecting_states))
			facts += facts_rejecting

			

	return facts


# Takes a formula of the form And, Not, Symbol, BooleanTrue, or BoolenFalse, and return the corresponding fact's body.
def guard_to_telingo_facts(guard) : 
	if isinstance(guard, And) : 
		line = ""
		for arg in guard.args : 
			line += guard_to_telingo_facts(arg)
		return line
	elif isinstance(guard, Not) :
		arg = guard.args[0]
		assert(isinstance(arg, Symbol))
		return ", not {}".format(str(arg))
	elif isinstance(guard, Symbol) : 
		return ", {}".format(str(guard))
	elif isinstance(guard, BooleanTrue) : 
		return ", #true"
	else : 
		assert(isinstance(guard, BooleanFalse))
		return ", #false"
	
	

# Takes a formula of the form And, Not, Symbol, BooleanTrue, or BoolenFalse, and return the corresponding fact's body.
def guard_to_clingo_facts(guard) : 
	if isinstance(guard, And) : 
		line = ""
		for arg in guard.args : 
			line += guard_to_clingo_facts(arg)
		return line
	elif isinstance(guard, Not) :
		arg = guard.args[0]
		assert(isinstance(arg, Symbol))
		return ", not holccurs({}, T)".format(str(arg))
	elif isinstance(guard, Symbol) : 
		return ", holccurs({}, T)".format(str(guard))
	elif isinstance(guard, BooleanTrue) : 
		return ", #true"
	else : 
		assert(isinstance(guard, BooleanFalse))
		return ", #false"
	

# Takes a formula of the form And, Not, Symbol, BooleanTrue, or BoolenFalse, and return the corresponding fact's body.
def guard_to_clingo_facts_v2(guard) : 
	if isinstance(guard, And) : 
		line = ""
		for arg in guard.args : 
			line += guard_to_clingo_facts_v2(arg)
		return line
	elif isinstance(guard, Not) :
		arg = guard.args[0]
		assert(isinstance(arg, Symbol))
		return ", not {}TIMEHERE".format(str(arg))
	elif isinstance(guard, Symbol) : 
		return ", {}TIMEHERE".format(str(guard))
	elif isinstance(guard, BooleanTrue) : 
		return ", #true"
	else : 
		assert(isinstance(guard, BooleanFalse))
		return ", #false"


def guard_to_clingo_facts_v3(guard) : 
	if isinstance(guard, Or) : 
		return reduce(lambda g1,g2 : "or({}, {})".format(guard_to_clingo_facts_v3(g1), guard_to_clingo_facts_v3(g2)), guard.args)
	elif isinstance(guard, And) : 
		return reduce(lambda g1,g2 : "and({}, {})".format(guard_to_clingo_facts_v3(g1), guard_to_clingo_facts_v3(g2)), guard.args)
	elif isinstance(guard, Not) :
		arg = guard.args[0]
		return "neg({})".format(guard_to_clingo_facts_v3(arg))
		# assert(isinstance(arg, Symbol))
		# return ", neg({})".format(str(arg))
	elif isinstance(guard, Symbol) : 
		return "atom({})".format(str(guard))
	elif isinstance(guard, BooleanTrue) : 
		return "atom(true)"
	elif type(guard) == type("str") : 
		return guard
	else : 
		assert(isinstance(guard, BooleanFalse))
		return "atom(false)"
	


def ter2symb2(ap, ternary):
	"""Translate ternary output to symbolic."""
	expr = And()
	i = 0
	for value in ternary:
		if value == "1":
			expr = And(expr, ap[i] if isinstance(ap, tuple) else ap)
		elif value == "0":
			assert value == "0"
			expr = And(expr, Not(ap[i] if isinstance(ap, tuple) else ap))
		else:
			assert value == "X", "[ERROR]: the guard is not X"
		i += 1
	# if type(expr) == type("str") : 
	# 	print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
	# print('returning expr ', expr, "  of type ", type(expr))
	return expr


def simplify_guard2(guards):
	"""Make a big OR among guards and simplify them."""
	final = Or()
	for g in guards:
		final = Or(final, g)
	return simplify(final)




####


def compute_declare_assumption(s):
	"""Compute declare assumptions."""
	pairs = list(it.combinations(s, 2))

	if pairs:
		first_assumption = "~(ex1 y: 0<=y & y<=max($) & ~("
		for symbol in s:
			if symbol == s[-1]:
				first_assumption += "y in " + symbol + "))"
			else:
				first_assumption += "y in " + symbol + " | "

		second_assumption = "~(ex1 y: 0<=y & y<=max($) & ~("
		for pair in pairs:
			if pair == pairs[-1]:
				second_assumption += (
					"(y notin " + pair[0] + " | y notin " + pair[1] + ")));"
				)
			else:
				second_assumption += (
					"(y notin " + pair[0] + " | y notin " + pair[1] + ") & "
				)

		return first_assumption + " & " + second_assumption
	else:
		return None


def createMonafile(p: str, id):
	"""Write the .mona file."""
	try:
		with open("{}/automa_{id}.mona".format(PACKAGE_DIR, id=id), "w+") as file:
			file.write(p)
	except IOError:
		print("[ERROR]: Problem opening the automa.mona file!")


def invoke_mona(id):
	"""Execute the MONA tool."""
	command = "mona -q -w {}/automa_{id}.mona".format(PACKAGE_DIR, id=id)
	process = Popen(
		args=command,
		stdout=PIPE,
		stderr=PIPE,
		preexec_fn=os.setsid,
		shell=True,
		encoding="utf-8",
	)
	try:
		output, error = process.communicate(timeout=30)
		return str(output).strip()
	except TimeoutExpired:
		os.killpg(os.getpgid(process.pid), signal.SIGTERM)
		return False


def output2dot(mona_output):
	"""Parse the mona output or return the unsatisfiable dot."""
	if "Formula is unsatisfiable" in mona_output:
		return UNSAT_DOT
	else:
		return parse_mona(mona_output)


def UNSAT_FACTS(n) : 
	UNSAT_FACTS_STR = "state({}, 1). \ns0({}, 1).\nrejecting_state({}, 1).\nlooping_state({}, 1). \n:- 1=1.\n\n".format(n,n,n,n,n)
	return UNSAT_FACTS_STR


def output2facts(mona_output, n, mode):
	"""Parse the mona output or return the unsatisfiable dot."""
	# print(mona_output)
	if "Formula is unsatisfiable" in mona_output:
		return UNSAT_FACTS(n)
	else:
		return parse_mona_to_facts(mona_output, n, mode)

def to_dfa(f) -> str:
	"""Translate to deterministic finite-state automaton."""
	p = MonaProgram(f)
	mona_p_string = p.mona_program()
	want_to_print_mona_program = True
	if(want_to_print_mona_program) : 
		print("\nMONA PROGRAM : ")
		print(mona_p_string)
		print()
	createMonafile(mona_p_string)
	mona_dfa = invoke_mona()
	want_to_print_mona_dfa = True
	if(want_to_print_mona_dfa) :
		print("*************************************")
		print(mona_dfa)
		print("*************************************")
	return output2dot(mona_dfa)



import random
import os

def to_dfa_facts(f, n, mode) -> str:
	"""Translate to deterministic finite-state automaton."""

	p = MonaProgram(f)
	mona_p_string = p.mona_program()
	want_to_print_mona_program = False
	if(want_to_print_mona_program) : 
		print("\nMONA PROGRAM : ")
		print(mona_p_string)
		print()
	unique_id = str(hash(mona_p_string)) + "_" + str(random.randint(1000000000,9999999999)) #Very ugly, should use async module.
	# print(unique_id)
	createMonafile(mona_p_string, unique_id)
	mona_dfa = invoke_mona(unique_id)
	cmd_rm_mona = "rm {}/automa_{id}.mona".format(PACKAGE_DIR, id=unique_id)
	os.system(cmd_rm_mona)
	want_to_print_mona_dfa = False
	if(want_to_print_mona_dfa) :
		print("*************************************")
		print(mona_dfa)
		print("*************************************")
	return output2facts(mona_dfa, n, mode)
