import sys
"""
sys.path.reverse()
sys.path.append('..')
sys.path.reverse()
"""
sys.path = ['..'] + sys.path
from ltlf2dfa.parser.ltlf import LTLfParser

parser = LTLfParser()
formula_str = "G(a -> X b)"
formula = parser(formula_str)       # returns an LTLfFormula

print(formula)                      # prints "G(a -> X (b))"
